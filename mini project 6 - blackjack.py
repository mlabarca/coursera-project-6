# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")    

# initialize some useful global variables
in_play = False
outcome = ""
new_deal = ""
score = 0

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        self.card_list = []
    
    def __str__(self):
        Out = ""
        for card in self.card_list:
            Out += str(card)
            Out += " "
        return Out	

    def add_card(self, card):
        self.card_list.append(card)

    def get_value(self):
    # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        self.hand_value = 0
        Ace = 0
        for card in self.card_list:
            self.hand_value += VALUES[card.get_rank()]  
            if card.get_rank() == 'A':
                Ace += 1
        if Ace == False:
            return self.hand_value
        else:
            if self.hand_value + 10 <= 21:
                return self.hand_value + 10
            return self.hand_value
    def draw(self, canvas, pos):
        # draw a hand on the canvas, use the draw method for cards
        for card in self.card_list:
        # no need to draw split cards if this goes beyond 4.   
            if self.card_list.index(card) <= 4:
                card.draw(canvas,[pos[0] + pos[0]*self.card_list.index(card), pos[1]])     
        
         
# define deck class 
class Deck:
    def __init__(self):
        self.card_list = []
        #Filling the list of cards in deck.
        for suit in SUITS:
            for rank in RANKS:
                self.card_list.append(Card(suit,rank))
                
        
    def shuffle(self):
        # add cards back to deck and shuffle
        random.shuffle(self.card_list)
        
    def deal_card(self):
        # deal a card object from the deck
        return self.card_list.pop()
    
    def __str__(self):
        # return a string representing the deck
        Out = " "
        for card in self.card_list:
            Out += str(card)
            Out += " "
        return Out        	
#define event handlers for buttons
def deal():
    global outcome, in_play, deck, player_hand, dealer_hand, score, new_deal
    #Deleting new deal message so it goes back to black at new game
    new_deal = ""
    #Miss-Click checker
    if in_play == True:
        outcome = "Lost the round for cheating. Hit or stand?" 
        score -= 1
    else:    
        outcome = "Hit or stand?"
    #reshuffling and dealing
    deck = Deck()
    deck.shuffle()
    player_hand = Hand()
    dealer_hand = Hand()
    #it was either this dumb loop or copy pasting these two statements again
    for i in range(2):
        player_hand.add_card(deck.deal_card())
        dealer_hand.add_card(deck.deal_card())
  
    in_play = True

def hit():
    global in_play, player_hand, outcome, score, new_deal      
    # if the hand is in play, hit the player
    if in_play:
        player_hand.add_card(deck.deal_card())
        if player_hand.get_value() > 21:
            outcome = "You have Busted"
            new_deal = "New Deal?"
            in_play = False
            score -= 1
    else:
    #Missclick checker    
        outcome = "You're already Busted, or not dealed yet"    
    
       
    # if busted, assign a message to outcome, update in_play and score
       
def stand():
    global dealer_hand, in_play, outcome, score, new_deal
    if in_play:
    # if hand is in play, repeatedly hit dealer until his hand has value 17 or more
        while dealer_hand.get_value() < 17:
            dealer_hand.add_card(deck.deal_card())
   
    # assign a message to outcome, update in_play and score        
        if dealer_hand.get_value() > 21:
            outcome = "Dealer busted, you win!"
            new_deal = "New Deal?"
            in_play = False
            score += 1
            
        elif dealer_hand.get_value() > player_hand.get_value():
            outcome = "Dealer Beats your hand! You lose."
            new_deal = "New Deal?"
            in_play = False
            score -= 1
            
        elif dealer_hand.get_value() < player_hand.get_value(): 
            outcome = "Your hand beats the dealer's. You win!"
            new_deal = "New Deal?"
            in_play = False
            score += 1
            
        else:
            outcome = "Tie, dealer STILL wins. You lose."
            new_deal = "New Deal?"
            in_play = False
            score -= 1
    else:
        outcome = "You're already Busted, or not dealed yet"   

    

# draw handler    
def draw(canvas):
    global player_hand, dealer_hand, score
    #Static text
    canvas.draw_text("Blackjack", (250, 40), 25, "White","sans-serif")
    canvas.draw_text("Score: "+str(score), (500, 30), 20, "White","sans-serif")
    canvas.draw_text("Dealer", (100,130), 20, "Aqua", "sans-serif")
    canvas.draw_text("Player", (100,280), 20, "Navy", "sans-serif")
    
    #Interface and "messaging" text
    canvas.draw_text(outcome,(50,500),25, "White","sans-serif")
    canvas.draw_text(new_deal,(50,530),25, "White","sans-serif")
    
    #Dealer's cards 
    dealer_hand.draw(canvas,(100,150))
    if in_play:
        canvas.draw_image(card_back, CARD_BACK_CENTER, CARD_BACK_SIZE, [
        100 + CARD_BACK_CENTER[0], 150 + CARD_BACK_CENTER[1]], CARD_BACK_SIZE)        
    
    #player's cards
    player_hand.draw(canvas,(100,300))    

# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
#using deal here starts the game right away after running
#that's the behaviour shown in the lecture video 
deal()
frame.start()
